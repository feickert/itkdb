<!-- prettier-ignore -->
::: mkdocs-click
    :module: itkdb.commandline
    :command: entrypoint
    :prog_name: itkdb
    :depth: 0
