#!/usr/bin/env bash
printf '\xff\xd8' # SOI
printf '\xff\xe0' # APP0
printf  '\x00\x10'
printf  '\x4a\x46\x49\x46\x00\x01\x01\x01\x00\x48\x00\x48\x00\x00'
printf '\xff\xdb' # DQT
printf  '\x00\x43'
printf  '\x00'
printf  '\x03\x02\x02\x02\x02\x02\x03\x02'
printf  '\x02\x02\x03\x03\x03\x03\x04\x06'
printf  '\x04\x04\x04\x04\x04\x08\x06\x06'
printf  '\x05\x06\x09\x08\x0a\x0a\x09\x08'
printf  '\x09\x09\x0a\x0c\x0f\x0c\x0a\x0b'
printf  '\x0e\x0b\x09\x09\x0d\x11\x0d\x0e'
printf  '\x0f\x10\x10\x11\x10\x0a\x0c\x12'
printf  '\x13\x12\x10\x13\x0f\x10\x10\x10'
printf '\xff\xc9' # SOF
printf  '\x00\x0b'
printf  '\x08\x00\x01\x00\x01\x01\x01\x11\x00'
printf '\xff\xcc' # DAC
printf  '\x00\x06\x00\x10\x10\x05'
printf '\xff\xda' # SOS
printf  '\x00\x08'
printf  '\x01\x01\x00\x00\x3f\x00\xd2\xcf\x20'
printf '\xff\xd9' # EOI
